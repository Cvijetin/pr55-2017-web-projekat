﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Data
{
    public class CommentData
    {
        public static Dictionary<int, Comment> ReadComments()
        {
            string path = "~/App_Data/Comments.txt";

            Dictionary<int, Comment> comments = new Dictionary<int, Comment>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('~');
                Comment newComment = new Comment(
                        int.Parse(tokens[0]),
                        int.Parse(tokens[1]),
                        int.Parse(tokens[2]),
                        tokens[3],
                        int.Parse(tokens[4]),
                        bool.Parse(tokens[5]),
                        tokens[6],
                        bool.Parse(tokens[7])
                    );

                comments.Add(newComment.ID, newComment);
            }
            sr.Close();
            stream.Close();

            return comments;
        }

        public static void SaveComents(Dictionary<int, Comment> comments)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Comments.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Comment item in comments.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public static int NumberOfComments()
        {
            return ReadComments().Count;
        }

        public Dictionary<int, Comment> GetAll()
        {
            return ReadComments();
        }
    }
}
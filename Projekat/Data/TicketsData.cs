﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Data
{
    public class TicketsData
    {
        public static Dictionary<string, Ticket> ReadTickets()
        {
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            string path = HostingEnvironment.MapPath("~/App_Data/Tickets.txt");
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('~');

                Ticket ticket = new Ticket(
                    tokens[0],
                    int.Parse(tokens[1]),
                    DateTime.Parse(tokens[2]),
                    double.Parse(tokens[3]),
                    int.Parse(tokens[4]),
                    (ETicketStatus)Enum.Parse(typeof(ETicketStatus), tokens[5]),
                    (ETicketType)Enum.Parse(typeof(ETicketType), tokens[6]),
                    bool.Parse(tokens[7])
                    );

                tickets.Add(ticket.TicketID, ticket);
            }
            sr.Close();
            stream.Close();

            return tickets;
        }

        public static void SaveTickets(Dictionary<string, Ticket> tickets)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Tickets.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Ticket item in tickets.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public static int NumberOfTickets()
        {
            return ReadTickets().Count;
        }

        public Dictionary<string, Ticket> GetAll()
        {
            return ReadTickets();
        }
    }
}
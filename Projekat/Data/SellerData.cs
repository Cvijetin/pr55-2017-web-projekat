﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Data
{
    public class SellerData
    {
        public static Dictionary<int, Seller> ReadSellers(string path)
        {
            Dictionary<int, Seller> sellers = new Dictionary<int, Seller>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('~');
                Seller seller = new Seller(
                    int.Parse(tokens[0]),
                    tokens[1],
                    tokens[2],
                    tokens[3],
                    tokens[4],
                    (EGender)Enum.Parse(typeof(EGender), tokens[5]),
                    DateTime.ParseExact(tokens[6], "dd-mm-yyyy", CultureInfo.InvariantCulture),
                    (EUserRole)Enum.Parse(typeof(EUserRole), tokens[7]),
                    bool.Parse(tokens[8])
                    );

                sellers.Add(seller.ID, seller);
            }
            sr.Close();
            stream.Close();

            return sellers;
        }
        public static void SaveSellers(Dictionary<int, Seller> sellers)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Sellers.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Seller item in sellers.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public int NumberOfSellers()
        {
            return ReadSellers("~/App_Data/Sellers.txt").Count;
        }

        public Dictionary<int, Seller> GetAll()
        {
            return ReadSellers("~/App_Data/Sellers.txt");
        }
    }
}
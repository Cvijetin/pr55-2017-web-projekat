﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Data
{
    public class ManifestationData
    {

        public static Dictionary<int, Manifestation> ReadManifestations()
        {
            Dictionary<int, Manifestation> manifestations = new Dictionary<int, Manifestation>();
            string path = HostingEnvironment.MapPath("~/App_Data/Manifestations.txt");
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('~');
                Venue newVenue = new Venue(
                    tokens[6],
                    tokens[7],
                    int.Parse(tokens[8]),
                    bool.Parse(tokens[9])
                    );

                Manifestation manifestation = new Manifestation(
                    int.Parse(tokens[0]),
                    (EManifestationType)Enum.Parse(typeof(EManifestationType), tokens[1]),
                    int.Parse(tokens[2]),
                    DateTime.Parse(tokens[3]),
                    double.Parse(tokens[4]),
                    bool.Parse(tokens[5]),

                    newVenue,

                    tokens[10],
                    bool.Parse(tokens[11]),
                    tokens[12],
                    int.Parse(tokens[13]),
                    int.Parse(tokens[14]),
                    int.Parse(tokens[15])
                    );

                manifestations.Add(manifestation.ID, manifestation);
            }
            sr.Close();
            stream.Close();

            return manifestations;
        }

        public static void SaveManifestations(Dictionary<int, Manifestation> manifestations)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Manifestations.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Manifestation item in manifestations.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public static int NumberOfManifestions()
        {
            return ReadManifestations().Count;
        }

        public Dictionary<int, Manifestation> GetAll()
        {
            return ReadManifestations();
        }
    }
}
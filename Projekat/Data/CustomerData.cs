﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Data
{
    public class CustomerData
    {
        public static Dictionary<int, Customer> ReadCustomers(string path)
        {
            Dictionary<int, Customer> customers = new Dictionary<int, Customer>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('~');
                Customer customer = new Customer(
                    int.Parse(tokens[0]),
                    tokens[1],
                    tokens[2],
                    tokens[3],
                    tokens[4],
                    (EGender)Enum.Parse(typeof(EGender), tokens[5]),
                    DateTime.ParseExact(tokens[6], "dd-mm-yyyy", CultureInfo.InvariantCulture),
                    (EUserRole)Enum.Parse(typeof(EUserRole), tokens[7]),
                    double.Parse(tokens[8]),
                    (ECustomerType)Enum.Parse(typeof(ECustomerType), tokens[9]),
                    int.Parse(tokens[10]),
                    bool.Parse(tokens[11]),
                    bool.Parse(tokens[12])
                    );

                customers.Add(customer.ID, customer);
            }
            sr.Close();
            stream.Close();

            return customers;
        }

        public static void SaveCustomers(Dictionary<int, Customer> customers)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Customers.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Customer item in customers.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }
        public int NumberOfCustomers()
        {
            return ReadCustomers("~/App_Data/Customers.txt").Count;
        }

        public Dictionary<int, Customer> GetAll()
        {
            return ReadCustomers("~/App_Data/Customers.txt");
        }
    }
}
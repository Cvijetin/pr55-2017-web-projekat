﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Data
{
    public class ApplicationData
    {
        public AdministratorData Administrator { get; set; }
        public CustomerData Customer { get; set; }
        public SellerData Seller { get; set; }

        public ApplicationData()
        {
            Administrator = new AdministratorData();
            Customer = new CustomerData();
            Seller = new SellerData();
        }
    }
}
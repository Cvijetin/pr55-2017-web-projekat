﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Projekat.Data
{
    public class AdministratorData
    {
        public static Dictionary<int, Administrator> ReadAdministrators(string path)
        {
            Dictionary<int, Administrator> admins = new Dictionary<int, Administrator>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('~');
                Administrator admin = new Administrator(
                    int.Parse(tokens[0]),
                    tokens[1],
                    tokens[2],
                    tokens[3],
                    tokens[4],
                    (EGender)Enum.Parse(typeof(EGender), tokens[5]),
                    DateTime.ParseExact(tokens[6], "MM-dd-yyyy", CultureInfo.InvariantCulture),
                    (EUserRole)Enum.Parse(typeof(EUserRole), tokens[7])
                    );

                admins.Add(admin.ID, admin);
            }
            sr.Close();
            stream.Close();

            return admins;
        }

        public static void SaveAdministrator(Dictionary<int, Administrator> admins)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Admins.txt");
            using (StreamWriter sw = File.CreateText(path))
            {
                foreach (Administrator item in admins.Values)
                {
                    sw.WriteLine(item);
                }
            }
        }

        public int NumberOfAdmins()
        {
            return ReadAdministrators("~/App_Data/Admins.txt").Count;
        }

        public Dictionary<int, Administrator> GetAll()
        {
            return ReadAdministrators("~/App_Data/Admins.txt");
        }
    }
}
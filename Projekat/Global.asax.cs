﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Projekat
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Dictionary<int, Administrator> admins = Data.AdministratorData.ReadAdministrators("~/App_Data/Admins.txt");
            HttpContext.Current.Application["admins"] = admins;

            Dictionary<int, Customer> customers = Data.CustomerData.ReadCustomers("~/App_Data/Customers.txt");
            HttpContext.Current.Application["customers"] = customers;

            Dictionary<int, Seller> sellers = Data.SellerData.ReadSellers("~/App_Data/Sellers.txt");
            HttpContext.Current.Application["sellers"] = sellers;

            Dictionary<int, Manifestation> manifestations = Data.ManifestationData.ReadManifestations();
            HttpContext.Current.Application["manifestations"] = manifestations;

            Dictionary<string, Ticket> tickets = Data.TicketsData.ReadTickets();
            HttpContext.Current.Application["tickets"] = tickets;

            Dictionary<int, Comment> comments = Data.CommentData.ReadComments();
            HttpContext.Current.Application["comments"] = comments;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Seller : User
    {
        public bool IsDeleted { get; set; }
        public Seller()
        {

        }

        public Seller(int iD, string username, string password, string name, string surname, EGender gender, DateTime birthDate, EUserRole userRole, bool isDeleted)
        {
            ID = iD;
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            Gender = gender;
            BirthDate = birthDate;
            UserRole = EUserRole.SELLER;
            IsDeleted = isDeleted;
        }

        public override string ToString()
        {
            return $"{ID}~{Username}~{Password}~{Name}~{Surname}~{Gender.ToString()}~" +
                $"{BirthDate.ToString("dd-MM-yyyy") ?? ""}~{EUserRole.SELLER.ToString()}~{IsDeleted.ToString()}";
        }
    }
}
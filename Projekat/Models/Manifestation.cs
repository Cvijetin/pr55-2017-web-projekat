﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Manifestation
    {
        public int ID { get; set; }   //10 karaktera mora da bude ID karte
        public EManifestationType Type { get; set; }
        public int Capacity { get; set; }
        public DateTime Date { get; set; }
        public double TicketPrice { get; set; }
        public bool Status { get; set; } //true -> Active, false -> Inactive
        public Venue Venue { get; set; }
        public string Poster { get; set; } //Putanja do slike
        public bool IsDeleted { get; set; }
        public string EventName { get; set; }
        public int Rating { get; set; }
        public int PurchasedCards { get; set; }
        public int SellerID { get; set; }


        public Manifestation(int iD, EManifestationType type, int capacity, DateTime date, double ticketPrice, bool status, Venue venue, string poster,
            bool isDeleted, string eventName, int rating, int purchasedCards, int sellerID)
        {
            ID = iD;
            Type = type;
            Capacity = capacity;
            Date = date;
            TicketPrice = ticketPrice;
            Status = status;
            Venue = venue;
            Poster = poster;
            IsDeleted = isDeleted;
            EventName = eventName;
            Rating = rating;
            PurchasedCards = purchasedCards;
            SellerID = sellerID;
        }

        public Manifestation()
        {

        }
        public override string ToString()
        {
            return $"{ID}~{Type.ToString()}~{Capacity}~{Convert.ToString(string.Format("{0:MM-dd-yyyy HH:mm tt}", Date))}~{TicketPrice}~" +
                $"{Status.ToString()}~{Venue.ToString()}~{Poster}~{IsDeleted}~{EventName}~{Rating}~{PurchasedCards}~{SellerID}";
        }
    }
}
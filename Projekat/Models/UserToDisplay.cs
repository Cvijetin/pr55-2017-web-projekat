﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class UserToDisplay
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public EGender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public EUserRole UserRole { get; set; }
        public double? NumberOfPoints { get; set; }
        public ECustomerType? CustomerType { get; set; }
        public int? NumberOfCancelations { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsSuspicious { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Customer : User
    {

        public double NumberOfPoints { get; set; }
        public ECustomerType CustomerType { get; set; }
        public int NumberOfCancelations { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsSuspicious { get; set; }

        public Customer()
        {

        }

        public Customer(int iD, string username, string password, string name, string surname, EGender gender, DateTime birthDate, EUserRole userRole,
            double numberOfPoints, ECustomerType customerType, int numberOfCancelations, bool isDeleted, bool isSuspicious) :
            base(iD, username, password, name, surname, gender, birthDate, userRole)
        {
            ID = iD;
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            Gender = gender;
            BirthDate = birthDate;
            UserRole = userRole;
            NumberOfPoints = numberOfPoints;
            CustomerType = customerType;
            NumberOfCancelations = numberOfCancelations;
            IsDeleted = isDeleted;
            IsSuspicious = isSuspicious;
        }

        public override string ToString()
        {
            return $"{ID}~{Username}~{Password}~{Name}~{Surname}~{Gender.ToString()}~" +
                $"{BirthDate.ToString("dd-MM-yyyy") ?? ""}~{EUserRole.CUSTOMER.ToString()}~" +
                $"{NumberOfPoints}~{CustomerType.ToString()}~{NumberOfCancelations}~{IsDeleted.ToString()}~{IsSuspicious.ToString()}";
        }
    }
}
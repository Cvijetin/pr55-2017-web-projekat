﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Location
    {

        public int ID { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int VenueID { get; set; }
        public bool IsDeleted { get; set; }

        public Location()
        {

        }

        public Location(double longitude, double latitude, int venueID, bool isDeleted)
        {
            Longitude = longitude;
            Latitude = latitude;
            VenueID = venueID;
            IsDeleted = isDeleted;
        }

        public override string ToString()
        {
            return $"{ID}~{Longitude}~{Latitude}~{VenueID}~{IsDeleted}";
        }
    }
}
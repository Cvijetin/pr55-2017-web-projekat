﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Venue
    {
        public string Address { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public bool IsDeleted { get; set; }

        public Venue(string address, string city, int postalCode, bool isDeleted)
        {
            Address = address;
            City = city;
            PostalCode = postalCode;
            IsDeleted = false;
        }

        public override string ToString()
        {
            return $"{Address}~{City}~{PostalCode}~{IsDeleted}";
        }
    }
}
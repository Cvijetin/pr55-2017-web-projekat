﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public EGender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public EUserRole UserRole { get; set; }

        public User()
        {

        }

        public User(int iD, string username, string password, string name, string surname, EGender gender, DateTime birthDate, EUserRole userRole)
        {
            ID = iD;
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            Gender = gender;
            BirthDate = birthDate;
            UserRole = userRole;
        }
    }
}
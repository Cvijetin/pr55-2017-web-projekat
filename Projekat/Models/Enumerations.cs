﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public enum ECustomerType
    {
        BRONZE,
        SILVER,
        GOLD
    }
    public enum EGender
    {
        MALE,
        FEMALE
    }
    public enum EManifestationType
    {
        CONCERT,
        FESTIVAL,
        THEATER,
        OTHER
    }
    public enum ETicketStatus
    {
        RESERVED,
        CANCELED
    }
    public enum ETicketType
    {
        VIP,
        REGULAR,
        FAN_PIT
    }
    public enum EUserRole
    {
        GUEST,
        ADMINISTRATOR,
        SELLER,
        CUSTOMER
    }
}
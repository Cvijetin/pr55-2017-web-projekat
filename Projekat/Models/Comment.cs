﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public int ManifestationID { get; set; }
        public string Text { get; set; }
        public int Rating { get; set; } //ocena od 1 do 5
        public bool IsDeleted { get; set; }
        public string TicketID { get; set; }
        public bool Accepted { get; set; } //po default-u je false, znaci da kupac mora prvenstveno da odobri manifestaciju!

        public Comment(int iD, int customerID, int manifestationID, string text, int rating, bool isDeleted, string ticketID, bool accepted)
        {
            ID = iD;
            CustomerID = customerID;
            ManifestationID = manifestationID;
            Text = text;
            Rating = rating;
            IsDeleted = isDeleted;
            TicketID = ticketID;
            Accepted = accepted; //needs still to be accepted!
        }
        public Comment()
        {

        }

        public override string ToString()
        {
            return $"{ID}~{CustomerID}~{ManifestationID}~{Text}~{Rating}~{IsDeleted.ToString()}~{TicketID}~{Accepted.ToString()}";
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class CustomerType
    {
        public int ID { get; set; }
        public ECustomerType Type { get; set; }

        public double Discount { get; set; }
        public int Points { get; set; }

        public CustomerType()
        {

        }

        public CustomerType(ECustomerType type, double discount, int points)
        {
            Type = type;
            Discount = discount;
            Points = points;
        }

        public override string ToString()
        {
            return $"{ID}~{Type.ToString()}~{Discount}~{Points}";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Ticket
    {
        public string TicketID { get; set; }
        public int ManifestationID { get; set; }
        public DateTime ManifestationDate { get; set; }
        public double Price { get; set; }
        public int CustomerID { get; set; } //name/surname

        public ETicketStatus Status { get; set; } 
        public ETicketType TicketType { get; set; }
        public bool IsDeleted { get; set; }

        public Ticket(string ticketID, int manifestationID, DateTime manifestationDate, double price, int customerID, ETicketStatus status, ETicketType ticketType, bool isDeleted)
        {
            TicketID = ticketID;
            ManifestationID = manifestationID;
            ManifestationDate = manifestationDate;
            Price = price;
            CustomerID = customerID;
            Status = status;
            TicketType = ticketType;
            IsDeleted = false;
        }
        public Ticket()
        {

        }

        public override string ToString()
        {
            return $"{TicketID}~{ManifestationID}~{ManifestationDate.ToString()}" +
                $"~{Price}~{CustomerID}~{Status.ToString()}~{TicketType.ToString()}~{IsDeleted.ToString()}";
        }

    }
}
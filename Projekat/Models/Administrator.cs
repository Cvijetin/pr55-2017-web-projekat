﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.Models
{
    public class Administrator : User
    {
        public Administrator()
        {

        }
        public Administrator(int iD, string username, string password, string name, string surname, EGender gender, DateTime birthDate, EUserRole userRole)
        {
            ID = iD;
            Username = username;
            Password = password;
            Name = name;
            Surname = surname;
            Gender = gender;
            BirthDate = birthDate;
            UserRole = EUserRole.ADMINISTRATOR;
        }
        public override string ToString()
        {
            return $"{ID}~{Username}~{Password}~{Name}~{Surname}~{Gender.ToString()}" +
                $"~{BirthDate.ToString("mm/dd/yyyy") ?? ""}~{EUserRole.ADMINISTRATOR.ToString()}";
        }
    }
}
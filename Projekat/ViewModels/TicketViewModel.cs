﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.ViewModels
{
    public class TicketViewModel
    {
        public int ManifestationID { get; set; }
        public int CustomerID { get; set; } 
        public DateTime ManifestationDate { get; set; }
        public double Price { get; set; }
        public int RegularCards { get; set; }
        public int FanPitCards { get; set; }
        public int VipCards { get; set; }
        public int NumberOfCards { get; set; }
    }
}
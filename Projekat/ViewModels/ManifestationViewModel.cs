﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.ViewModels
{
    public class ManifestationViewModel
    {
        public string EventName { get; set; }
        public EManifestationType Type { get; set; }
        public int Capacity { get; set; }
        public DateTime Date { get; set; }
        public double TicketPrice { get; set; }
        public bool Status { get; set; } //true -> Active, false -> Inactive
        public string Address { get; set; }
        public string City { get; set; }
        public int PostalCode { get; set; }
        public string Poster { get; set; } //Putanja do slike
        public int Rating { get; set; }
        public int SelelrID { get; set; }

    }
}
﻿using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projekat.ViewModels
{
    public class ProfileViewModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public EGender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public EUserRole UserRole { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPasswrod { get; set; }
        public double? NumberOfCollectedPoints { get; set; }
        public ECustomerType? CustomerType { get; set; }
    }
}
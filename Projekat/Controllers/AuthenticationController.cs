﻿using Projekat.Data;
using Projekat.Models;
using Projekat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class AuthenticationController : Controller
    {
        private ApplicationData app_data;

        public AuthenticationController()
        {
            HomeController homeController = new HomeController();
            app_data = new ApplicationData();
        }
        // GET: Authentification
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register() //Samo se kupci mogu registrovati, prodavaoce dodaju administratori, administratori su rucno zakucani
        {
            Customer customer = (Customer)Session["customer"];
            if (customer != null && !customer.Username.Equals(string.Empty))
                return Content("<h1>Error!<h1>");

            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Application["customers"];

            Customer customer = new Customer()
            {
                ID = ++HomeController.customerID,
                Username = model.Username,
                Name = model.Name,
                Surname = model.Surname,
                Password = model.Password,
                Gender = model.Gender,
                BirthDate = model.BirthDate,
                UserRole = EUserRole.CUSTOMER
            };

            foreach (var item in customers.Values)
            {
                if (customer.Username.Equals(item.Username))
                {
                    ViewBag.Message = "Username already Exists!";
                }
            }

            customers.Add(customer.ID, customer);
            CustomerData.SaveCustomers(customers);

            return RedirectToAction("Login", "Authentication");
        }

        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Message = TempData["errorMessage"];

            Customer customer = (Customer)Session["customer"];
            Seller seller = (Seller)Session["seller"];
            Administrator admin = (Administrator)Session["admin"];

            if (customer != null && !(customer.Username.Equals(string.Empty)))
                return Content("Error, already logged in!");
            else if (seller != null && !(seller.Username.Equals(string.Empty)))
                return Content("Error, already logged in!");
            else if (admin != null && !(admin.Username.Equals(string.Empty)))
                return Content("Error, already logged in!");
            else
                return View();

        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (CheckRole(model.Username, model.Password) == EUserRole.CUSTOMER)
            {
                Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Application["customers"];
                Customer temp = new Customer();
                foreach (var item in customers.Values)
                {
                    if (item.Username.Equals(model.Username) && item.Password.Equals(model.Password))
                    {
                        temp = item;
                        break;
                    }
                }

                if (temp.IsDeleted)
                {
                    ViewBag.Message = $"{temp.Username}'s Account is blocked!";
                    return View();
                }

                Session["customer"] = temp;
            }
            else if (CheckRole(model.Username, model.Password) == EUserRole.ADMINISTRATOR)
            {
                Dictionary<int, Administrator> admins = (Dictionary<int, Administrator>)HttpContext.Application["admins"];
                Administrator temp = new Administrator();
                foreach (var item in admins.Values)
                {
                    if (item.Username.Equals(model.Username) && item.Password.Equals(model.Password))
                    {
                        temp = item;
                        break;
                    }
                }
                Session["admin"] = temp;
            }
            else if (CheckRole(model.Username, model.Password) == EUserRole.SELLER)
            {
                Dictionary<int, Seller> sellers = (Dictionary<int, Seller>)HttpContext.Application["sellers"];
                Seller temp = new Seller();
                foreach (var item in sellers.Values)
                {
                    if (item.Username.Equals(model.Username) && item.Password.Equals(model.Password))
                    {
                        temp = item;
                        break;
                    }
                }

                if (temp.IsDeleted)
                {
                    ViewBag.Message = $"{temp.Username}'s Account is blocked!";
                    return View();
                }

                Session["seller"] = temp;
            }
            else
            {
                ViewBag.Message = $"Wrong username or password!";
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut()
        {

            Customer customer = (Customer)Session["customer"];
            Administrator admin = (Administrator)Session["admin"];
            Seller seller = (Seller)Session["seller"];
            if (customer != null)
            {
                Session["customer"] = customer;
                Session["customer"] = null;
            }
            else if (admin != null)
            {
                Session["admin"] = admin;
                Session["admin"] = null;
            }
            else if (seller != null)
            {
                Session["seller"] = seller;
                Session["seller"] = null;
            }

            return RedirectToAction("Index", "Home");
        }
        public ActionResult ProfileSettings()
        {
            ProfileViewModel model = new ProfileViewModel();

            if (IsAdministrator())
            {
                Administrator admin = (Administrator)Session["admin"];
                model = new ProfileViewModel
                {
                    ID = admin.ID,
                    Username = admin.Username,
                    Password = admin.Password,
                    Name = admin.Name,
                    Surname = admin.Surname,
                    Gender = admin.Gender,
                    BirthDate = admin.BirthDate,
                    UserRole = admin.UserRole,
                    NewPassword = "",
                    ConfirmPasswrod = "",
                    NumberOfCollectedPoints = null,
                    CustomerType = null
                };

                ViewBag.Layout = "~/Views/Shared/_Layout_Admin.cshtml";
            }
            else if (IsSeller())
            {
                Seller seller = (Seller)Session["seller"];
                model = new ProfileViewModel
                {
                    ID = seller.ID,
                    Username = seller.Username,
                    Password = seller.Password,
                    Name = seller.Name,
                    Surname = seller.Surname,
                    Gender = seller.Gender,
                    BirthDate = seller.BirthDate,
                    UserRole = seller.UserRole,
                    NewPassword = "",
                    ConfirmPasswrod = "",
                    NumberOfCollectedPoints = null,
                    CustomerType = null
                };

                ViewBag.Layout = "~/Views/Shared/_Layout_Seller.cshtml";
            }
            else if (isCusromer())
            {
                Customer customer = (Customer)Session["customer"];
                model = new ProfileViewModel
                {
                    ID = customer.ID,
                    Username = customer.Username,
                    Password = customer.Password,
                    Name = customer.Name,
                    Surname = customer.Surname,
                    Gender = customer.Gender,
                    BirthDate = customer.BirthDate,
                    UserRole = customer.UserRole,
                    NewPassword = "",
                    ConfirmPasswrod = "",
                    NumberOfCollectedPoints = customer.NumberOfPoints,
                    CustomerType = customer.CustomerType
                };

                ViewBag.Layout = "~/Views/Shared/_Layout_Customer.cshtml";
            }
            else
            {
                return Content("<h1>You have no premision for this action!</h1>");
            }

            return View(model);
        }
       
        private EUserRole CheckRole(string username, string password)
        {
            if (IsAdmin(username, password))
                return EUserRole.ADMINISTRATOR;
            else if (IsSeller(username, password))
                return EUserRole.SELLER;
            else if (IsCustomer(username, password))
                return EUserRole.CUSTOMER;
            else
                return EUserRole.GUEST;
        }

        private bool IsAdmin(string username, string password)
        {
            Dictionary<int, Administrator> admins = (Dictionary<int, Administrator>)HttpContext.Application["admins"];

            foreach (var item in admins.Values)
            {
                if (username.Equals(item.Username) && password.Equals(item.Password))
                {
                    return true;
                }
            }
            return false;
        }
        private bool IsAdministrator()
        {
            Administrator admin = (Administrator)Session["admin"];
            if (admin == null || admin.UserRole != EUserRole.ADMINISTRATOR || admin.Username == "")
                return false;

            return true;
        }
        private bool isCusromer()
        {
            Customer customer = (Customer)Session["customer"];
            if (customer == null || customer.UserRole != EUserRole.CUSTOMER || customer.Username == "")
                return false;

            return true;
        }
        private bool IsSeller()
        {
            Seller seller = (Seller)Session["seller"];
            if (seller == null || seller.UserRole != EUserRole.SELLER || seller.Username == "")
                return false;

            return true;
        }

        private bool IsSeller(string username, string password)
        {
            Dictionary<int, Seller> sellers = (Dictionary<int, Seller>)HttpContext.Application["sellers"];

            foreach (var item in sellers.Values)
            {
                if (username.Equals(item.Username) && password.Equals(item.Password))
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsCustomer(string username, string password)
        {
            Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Application["customers"];

            foreach (var item in customers.Values)
            {
                if (username.Equals(item.Username) && password.Equals(item.Password))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
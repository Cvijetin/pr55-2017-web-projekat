﻿using Projekat.Data;
using Projekat.Models;
using Projekat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class AdministratorController : Controller
    {
        private ApplicationData app_data;
        public AdministratorController()
        {
            app_data = new ApplicationData();
            HomeController homeController = new HomeController();
        }

        // GET: Administrator
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllUsers()
        {
            if (!IsAdministrator())
            {
                TempData["errorMessage"] = "Error!The user is not an ADMINISTRATOR";
                return RedirectToAction("Login", "Authentication");
            }

            List<UserToDisplay> UsersForDisplay = new List<UserToDisplay>();
            Dictionary<int, Administrator> admins = app_data.Administrator.GetAll().OrderBy(key => key.Value.Username).ToDictionary(x => x.Key, x => x.Value);
            Dictionary<int, Seller> sellers = app_data.Seller.GetAll().OrderBy(key => key.Value.Username).ToDictionary(x => x.Key, x => x.Value);
            Dictionary<int, Customer> customers = app_data.Customer.GetAll().OrderBy(key => key.Value.Username).ToDictionary(x => x.Key, x => x.Value);

            foreach (var item in admins.Values)
            {
                UsersForDisplay.Add(new UserToDisplay()
                {
                    ID = item.ID,
                    Username = item.Username,
                    Password = item.Password,
                    Name = item.Name,
                    Surname = item.Surname,
                    Gender = item.Gender,
                    BirthDate = item.BirthDate,
                    UserRole = EUserRole.ADMINISTRATOR,
                    NumberOfPoints = null,
                    CustomerType = null,
                    NumberOfCancelations = null,
                    IsDeleted = false,
                    IsSuspicious = null        
                });
            }

            foreach (var item in sellers.Values)
            {
                UsersForDisplay.Add(new UserToDisplay()
                {
                    ID = item.ID,
                    Username = item.Username,
                    Password = item.Password,
                    Name = item.Name,
                    Surname = item.Surname,
                    Gender = item.Gender,
                    BirthDate = item.BirthDate,
                    UserRole = EUserRole.SELLER,
                    NumberOfPoints = null,
                    CustomerType = null,
                    NumberOfCancelations = null,
                    IsDeleted = item.IsDeleted,
                    IsSuspicious = false
                });
            }

            foreach (var item in customers.Values)
            {
                UsersForDisplay.Add(new UserToDisplay()
                {
                    ID = item.ID,
                    Username = item.Username,
                    Password = item.Password,
                    Name = item.Name,
                    Surname = item.Surname,
                    Gender = item.Gender,
                    BirthDate = item.BirthDate,
                    UserRole = item.UserRole,
                    NumberOfPoints = item.NumberOfPoints,
                    CustomerType = item.CustomerType,
                    NumberOfCancelations = item.NumberOfCancelations,
                    IsDeleted = item.IsDeleted,
                    IsSuspicious = item.IsSuspicious
                });
            }

            return View(UsersForDisplay);
        }
        [HttpGet]
        public ActionResult CreateNewSeller()
        {
            if (!IsAdministrator())
            {
                return Content("<h1>You have no premision for this action!</h1>");
            }

            return View();
        }

        [HttpPost]
        public ActionResult CreateNewSeller(Seller model)
        {
            Dictionary<int, Seller> sellers = (Dictionary<int, Seller>)HttpContext.Application["sellers"];

            Seller seller = new Seller()
            {
                ID = ++HomeController.sellerID,
                Username = model.Username,
                Name = model.Name,
                Surname = model.Surname,
                Password = model.Password,
                Gender = model.Gender,
                BirthDate = model.BirthDate,
                UserRole = EUserRole.CUSTOMER,
                IsDeleted = false
            };


            foreach (var item in sellers.Values)
            {
                if (seller.Username.Equals(item.Username))
                {
                    TempData["Message"] = "Username already Exists! Try creating the seller again!";
                    return RedirectToAction("Index", "Home");
                }
            }

            sellers.Add(seller.ID, seller);
            SellerData.SaveSellers(sellers);

            return RedirectToAction("Index", "Home");
        }

        private bool IsAdministrator()
        {
            Administrator admin = (Administrator)Session["admin"];
            if (admin == null || admin.UserRole != EUserRole.ADMINISTRATOR || admin.Username == "")
                return false;

            return true;
        }
    }
}
﻿using Projekat.Data;
using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class SellerController : Controller
    {
        // GET: Seller
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManifestationDetails(int id)
        {
            Dictionary<int, Manifestation> manifestations = (Dictionary<int, Manifestation>)HttpContext.Application["manifestations"];
            Manifestation temp = new Manifestation();

            Dictionary<int, Comment> comments = new Dictionary<int, Comment>();
            comments = CommentData.ReadComments();

            Dictionary<int, Customer> customers = new Dictionary<int, Customer>();
            customers = CustomerData.ReadCustomers("~/App_Data/Customers.txt");

            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            tickets = TicketsData.ReadTickets();

            Ticket ticket = null;
            foreach (var item in tickets.Values)
            {
                if (item.ManifestationID.Equals(id))
                {
                    ticket = item;
                    break;
                }
            }

            foreach (var item in manifestations.Values)
            {
                if (item.ID.Equals(id))
                {
                    temp = item;
                    break;
                }
            }
            EUserRole role = CheckRole();
            if (role == EUserRole.ADMINISTRATOR)
            {
                ViewBag.Layout = "~/Views/Shared/_Layout_Admin.cshtml";
                Administrator admin = (Administrator)Session["admin"];
                ViewBag.User = admin;
            }
            else if (role == EUserRole.CUSTOMER)
            {
                ViewBag.Layout = "~/Views/Shared/_Layout_Customer.cshtml";
                Customer customer = (Customer)Session["customer"];
                ViewBag.User = customer;
            }
            else if (role == EUserRole.SELLER)
            {
                ViewBag.Layout = "~/Views/Shared/_Layout_Seller.cshtml";
                Seller seller = (Seller)Session["seller"];
                ViewBag.User = seller;

            }
            else //Guest
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
                ViewBag.User = null;
            }

            ViewBag.UserRole = role;
            ViewBag.Comments = comments;
            ViewBag.Customers = customers;
            ViewBag.Ticket = ticket;

            return View(temp);
        }

        private EUserRole CheckRole()
        {
            Administrator admin = (Administrator)Session["admin"];
            if (admin == null || admin.UserRole != EUserRole.ADMINISTRATOR || admin.UserRole.Equals(string.Empty))
            {
                Seller seller = (Seller)Session["seller"];
                if (seller == null || seller.UserRole != EUserRole.SELLER || seller.UserRole.Equals(string.Empty))
                {
                    Customer customer = (Customer)Session["customer"];
                    if (customer == null || customer.UserRole != EUserRole.CUSTOMER || customer.UserRole.Equals(string.Empty))
                    {
                        return EUserRole.GUEST;
                    }
                    return EUserRole.CUSTOMER;
                }
                return EUserRole.SELLER;
            }
            return EUserRole.ADMINISTRATOR;
        }
    }
}
﻿using Projekat.Models;
using Projekat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Projekat.Data;

namespace Projekat.Controllers
{
    [RoutePrefix("authentication")]
    public class ApiAuthenticationController : ApiController
    {
        [Route("editProfile")]
        [HttpPost]
        public string EditProfile([FromBody]ProfileViewModel model)
        {

            if (model.UserRole == EUserRole.ADMINISTRATOR)
            {
                Dictionary<int, Administrator> admins = (Dictionary<int, Administrator>)HttpContext.Current.Application["admins"];
                foreach (var item in admins.Values)
                {
                    if (item.Username.Equals(model.Username) && (item.ID != model.ID))
                    {
                        return "Username already exists!";
                    }
                }

                foreach (var item in admins.Values)
                {
                    if (item.ID == model.ID && model.UserRole.Equals(EUserRole.ADMINISTRATOR))
                    {
                        item.Username = model.Username;
                        item.Password = model.NewPassword;
                        item.Name = model.Name;
                        item.Surname = model.Surname;
                        item.Gender = model.Gender;
                        item.BirthDate = model.BirthDate;
                        item.UserRole = model.UserRole;
                        break;
                    }
                }
                AdministratorData.SaveAdministrator(admins);
                return "Successful";

            }
            else if (model.UserRole == EUserRole.CUSTOMER)
            {
                Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Current.Application["customers"];
                foreach (var item in customers.Values)
                {
                    if (item.Username.Equals(model.Username) && (item.ID != model.ID))
                    {
                        return "Username already exists!";
                    }
                }

                foreach (var item in customers.Values)
                {
                    if (item.ID == model.ID && model.UserRole.Equals(EUserRole.CUSTOMER))
                    {
                        item.Username = model.Username;
                        item.Password = model.NewPassword;
                        item.Name = model.Name;
                        item.Surname = model.Surname;
                        item.Gender = model.Gender;
                        item.BirthDate = model.BirthDate;
                        item.UserRole = model.UserRole;
                        break;
                    }
                }
                CustomerData.SaveCustomers(customers);
                return "Successful";
            }
            else if (model.UserRole == EUserRole.SELLER)
            {
                Dictionary<int, Seller> sellers = (Dictionary<int, Seller>)HttpContext.Current.Application["sellers"];
                foreach (var item in sellers.Values)
                {
                    if (item.Username.Equals(model.Username) && (item.ID != model.ID))
                    {
                        return "Username already exists!";
                    }
                }

                foreach (var item in sellers.Values)
                {
                    if (item.ID == model.ID && model.UserRole.Equals(EUserRole.SELLER))
                    {
                        item.Username = model.Username;
                        item.Password = model.NewPassword;
                        item.Name = model.Name;
                        item.Surname = model.Surname;
                        item.Gender = model.Gender;
                        item.BirthDate = model.BirthDate;
                        item.UserRole = model.UserRole;
                        break;
                    }
                }
                SellerData.SaveSellers(sellers);
                return "Successful";
            }
            else return "Error";
        }
    }
}

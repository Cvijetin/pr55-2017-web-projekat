﻿using Projekat.Data;
using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationData app_data;
        public static int administratorID = 0;
        public static int customerID = 0;
        public static int sellerID = 0;

        public HomeController()
        {
            app_data = new ApplicationData();
            administratorID = app_data.Administrator.NumberOfAdmins();
            customerID = app_data.Customer.NumberOfCustomers();
            sellerID = app_data.Seller.NumberOfSellers();
        }
        public ActionResult Index()
        {

            EUserRole role = CheckRole();
            if (role == EUserRole.ADMINISTRATOR)
                ViewBag.Layout = "~/Views/Shared/_Layout_Admin.cshtml";
            else if (role == EUserRole.CUSTOMER)
                ViewBag.Layout = "~/Views/Shared/_Layout_Customer.cshtml";
            else if (role == EUserRole.SELLER)
                ViewBag.Layout = "~/Views/Shared/_Layout_Seller.cshtml";
            else //Guest
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";

            ViewBag.UserRole = role;
            ViewBag.Message = TempData["Message"];


            if (role == EUserRole.SELLER)
            {
                Seller seller = (Seller)Session["seller"];
                ViewBag.SellerID = seller.ID;
            }

            Dictionary<int, Manifestation> manifestations = ManifestationData.ReadManifestations();
            Dictionary<int, Comment> comments = CommentData.ReadComments();

            foreach (var manifestation in manifestations.Values)
            {
                int ratings = 0;
                int amount = 0;
                foreach (var comment in comments.Values)
                {
                    if (manifestation.ID.Equals(comment.ManifestationID) && comment.Accepted && !comment.IsDeleted)
                    {
                        amount += 1;
                        ratings += comment.Rating;
                    }
                }
                if (amount == 0 || ratings == 0)
                { }
                else
                    manifestation.Rating = ratings / amount;

            }


            Dictionary<int, Manifestation> orderedManifestations = manifestations.OrderByDescending(b => b.Value.Date).ToDictionary(t => t.Key, t => t.Value);
            ViewBag.ManifestationsCount = ManifestationData.NumberOfManifestions();
            return View(orderedManifestations);
        }


        public ActionResult PurchaseCard(int id)
        {
            Dictionary<int, Manifestation> manifestations = ManifestationData.ReadManifestations();
            Manifestation temp = new Manifestation();
            foreach (var item in manifestations.Values)
            {
                if (item.ID.Equals(id))
                {
                    temp = item;
                    break;
                }
            }

            Customer customer = (Customer)Session["customer"];
            ViewBag.CustomerID = customer.ID;
            ViewBag.CustomerType = customer.CustomerType;

            return View(temp);

        }

        private EUserRole CheckRole()
        {
            Administrator admin = (Administrator)Session["admin"];
            if (admin == null || admin.UserRole != EUserRole.ADMINISTRATOR || admin.UserRole.Equals(string.Empty))
            {
                Seller seller = (Seller)Session["seller"];
                if (seller == null || seller.UserRole != EUserRole.SELLER || seller.UserRole.Equals(string.Empty))
                {
                    Customer customer = (Customer)Session["customer"];
                    if (customer == null || customer.UserRole != EUserRole.CUSTOMER || customer.UserRole.Equals(string.Empty))
                    {
                        return EUserRole.GUEST;
                    }
                    return EUserRole.CUSTOMER;
                }
                return EUserRole.SELLER;
            }
            return EUserRole.ADMINISTRATOR;
        }
    }
}

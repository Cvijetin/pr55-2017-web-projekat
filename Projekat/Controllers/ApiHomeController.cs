﻿using Projekat.Data;
using Projekat.Models;
using Projekat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Projekat.Controllers
{
    [RoutePrefix("home")]
    public class ApiHomeController : ApiController
    {
        [Route("createManifestation")]
        [HttpPost]
        public string CreateManifestation([FromBody]ManifestationViewModel model)
        {
            Dictionary<int, Manifestation> manifestations = (Dictionary<int, Manifestation>)HttpContext.Current.Application["manifestations"];

            foreach (var item in manifestations.Values)
            {
                if (model.Date.Equals(item.Date) || model.Address.Equals(item.Venue.Address))
                {
                    return "Manifestation already exists";
                }
            }

            int currentNumberOfManifestaiton = ManifestationData.NumberOfManifestions();
            bool deleted = false;

            string[] image = model.Poster.Split('\\');

            Manifestation newManifestation = new Manifestation()
            {
                ID = ++currentNumberOfManifestaiton,
                Type = model.Type,
                Capacity = model.Capacity,
                Date = model.Date,
                TicketPrice = model.TicketPrice,
                Status = false, //by default every manifestation that is created is INACTIVE (boolean False)
                Venue = new Venue(model.Address, model.City, model.PostalCode, deleted)
                {
                    Address = model.Address,
                    City = model.City,
                    PostalCode = model.PostalCode,
                    IsDeleted = false
                },
                Poster = image[2],
                IsDeleted = false, //by default every manifestation is still in use
                EventName = model.EventName,
                Rating = 0,
                PurchasedCards = 0,
                SellerID = model.SelelrID
            };

            manifestations.Add(newManifestation.ID, newManifestation);
            ManifestationData.SaveManifestations(manifestations);

            return "Successful";
        }
    }
}
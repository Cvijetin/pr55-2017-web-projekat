﻿using Projekat.Data;
using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Projekat.Controllers
{
    [RoutePrefix("administrator")]
    public class ApiAdministratorController : ApiController
    {
        public ApiAdministratorController()
        {

        }

        [Route("delete")]
        [HttpGet]
        public string Delete([FromUri]string id, [FromUri]string userRole)
        {
            if (userRole.Equals(EUserRole.SELLER.ToString()))
            {
                Dictionary<int, Seller> sellers = (Dictionary<int, Seller>)HttpContext.Current.Application["sellers"];
                foreach (var item in sellers.Values)
                {
                    if (int.Parse(id).Equals(item.ID))
                    {
                        item.IsDeleted = true;
                        break;
                    }
                }
                SellerData.SaveSellers(sellers);
                return "Success";
            }
            else if (userRole.Equals(EUserRole.CUSTOMER.ToString()))
            {
                Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Current.Application["customers"];
                foreach (var item in customers.Values)
                {
                    if (int.Parse(id).Equals(item.ID))
                    {
                        item.IsDeleted = true;
                        break;
                    }
                }
                CustomerData.SaveCustomers(customers);
                return "Success";
            }
            else
                return "Error";

        }

        [Route("unDelete")]
        [HttpGet]
        public string UnDelete([FromUri]string id, [FromUri]string userRole)
        {
            if (userRole.Equals(EUserRole.SELLER.ToString()))
            {
                Dictionary<int, Seller> sellers = (Dictionary<int, Seller>)HttpContext.Current.Application["sellers"];
                foreach (var item in sellers.Values)
                {
                    if (int.Parse(id).Equals(item.ID))
                    {
                        item.IsDeleted = false;
                        break;
                    }
                }
                SellerData.SaveSellers(sellers);
                return "Success";
            }
            else if (userRole.Equals(EUserRole.CUSTOMER.ToString()))
            {
                Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Current.Application["customers"];
                foreach (var item in customers.Values)
                {
                    if (int.Parse(id).Equals(item.ID))
                    {
                        item.IsDeleted = false;
                        break;
                    }
                }
                CustomerData.SaveCustomers(customers);
                return "Success";
            }
            else
                return "Error";
        }

        [Route("approveManifestation")]
        [HttpGet]
        public string ApproveManifestation([FromUri]int ID)
        {
            Dictionary<int, Manifestation> manifestations = (Dictionary<int, Manifestation>)HttpContext.Current.Application["manifestations"];

            manifestations[ID].Status = true;
            ManifestationData.SaveManifestations(manifestations);

            return "Successful";
        }

        [Route("deleteManifestation")]
        [HttpGet]
        public string DeleteManifestation([FromUri]string id)
        {

            Dictionary<int, Manifestation> manifestations = (Dictionary<int, Manifestation>)HttpContext.Current.Application["manifestations"];
            foreach (var item in manifestations.Values)
            {
                if (item.ID.Equals(int.Parse(id)))
                {
                    item.IsDeleted = true;
                    ManifestationData.SaveManifestations(manifestations);
                    return "Successfull";
                }
            }

            return "Error";
        }

        [Route("unDeleteManifestation")]
        [HttpGet]
        public string UnDeleteManifestation([FromUri]string id)
        {

            Dictionary<int, Manifestation> manifestations = (Dictionary<int, Manifestation>)HttpContext.Current.Application["manifestations"];
            foreach (var item in manifestations.Values)
            {
                if (item.ID.Equals(int.Parse(id)))
                {
                    item.IsDeleted = false;
                    ManifestationData.SaveManifestations(manifestations);
                    return "Successfull";
                }
            }

            return "Error";
        }

        [Route("deleteComment")]
        [HttpGet]
        public string DeleteComment([FromUri]int id)
        {

            Dictionary<int, Comment> comments = (Dictionary<int, Comment>)HttpContext.Current.Application["comments"];
            foreach (var item in comments.Values)
            {
                if (item.ID.Equals(id))
                {
                    item.IsDeleted = true;
                    CommentData.SaveComents(comments);
                    return "Successfull";
                }
            }

            return "Error";
        }

        [Route("unDeleteComment")]
        [HttpGet]
        public string UnDeleteComment([FromUri]int id)
        {

            Dictionary<int, Comment> comments = (Dictionary<int, Comment>)HttpContext.Current.Application["comments"];
            foreach (var item in comments.Values)
            {
                if (item.ID.Equals(id))
                {
                    item.IsDeleted = false;
                    CommentData.SaveComents(comments);
                    return "Successfull";
                }
            }

            return "Error";
        }

    }
}

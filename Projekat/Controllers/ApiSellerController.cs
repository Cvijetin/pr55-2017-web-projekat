﻿using Projekat.Data;
using Projekat.Models;
using Projekat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Projekat.Controllers
{
    [RoutePrefix("seller")]
    public class ApiSellerController : ApiController
    {
        [Route("editManifestation")]
        [HttpPost]
        public string EditManifestation([FromBody]Manifestation model)
        {
            Dictionary<int, Manifestation> manifestations = (Dictionary<int, Manifestation>)HttpContext.Current.Application["manifestations"];
            string[] image = model.Poster.Split('\\');
            foreach (var item in manifestations.Values)
            {
                if(item.ID == model.ID){
                    item.EventName = model.EventName;
                    item.Type = model.Type;
                    item.Capacity = model.Capacity;
                    item.Venue = model.Venue;
                    item.Poster = image[2];
                    item.TicketPrice = model.TicketPrice;
                    item.Date = model.Date;
                    item.Rating = model.Rating;

                    ManifestationData.SaveManifestations(manifestations);
                    return "Successful";
                }
            }
            return "Error";
        }

        [Route("createComment")]
        [HttpPost]
        public string CreateComment([FromBody]Comment model)
        {

            Dictionary<int, Comment> comments = (Dictionary<int, Comment>)HttpContext.Current.Application["comments"];
            foreach (var item in comments.Values)
            {
                if (item.ID.Equals(model.ID))
                {
                    return "Error";
                }
            }

            int number = CommentData.NumberOfComments();

            Comment newComment = new Comment() {
                ID = ++number,
                CustomerID = model.CustomerID,
                ManifestationID = model.ManifestationID,
                Text = model.Text,
                Rating = model.Rating,
                IsDeleted = false,
                TicketID = model.TicketID,
                Accepted = false
            };

            comments.Add(newComment.ID, newComment);
            CommentData.SaveComents(comments);
            return "Successful";
        }

        [Route("approveComment")]
        [HttpGet]
        public string ApproveComment([FromUri]int ID)
        {
            Dictionary<int, Comment> comments = (Dictionary<int, Comment>)HttpContext.Current.Application["comments"];

            comments[ID].Accepted = true;
            CommentData.SaveComents(comments);

            return "Successful";
        }


    }
}

﻿using Projekat.Data;
using Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekat.Controllers
{
    public class CardsController : Controller
    {
        // GET: Cards
        public ActionResult AllCards()
        {

            Dictionary<string, Ticket> tickets = (Dictionary<string, Ticket>)HttpContext.Application["tickets"];

            Dictionary<int, Manifestation> manifestations = new Dictionary<int, Manifestation>();
            manifestations = ManifestationData.ReadManifestations();

            Dictionary<int, Customer> customers = new Dictionary<int, Customer>();
            customers = CustomerData.ReadCustomers("~/App_Data/Customers.txt");


            EUserRole role = CheckRole();
            if (role == EUserRole.ADMINISTRATOR)
            {
                ViewBag.Layout = "~/Views/Shared/_Layout_Admin.cshtml";
            }
            else if (role == EUserRole.CUSTOMER)
            {
                ViewBag.Layout = "~/Views/Shared/_Layout_Customer.cshtml";
                Customer customer = (Customer)Session["customer"];
                ViewBag.Customer = customer;
            }
            else if (role == EUserRole.SELLER)
            {
                ViewBag.Layout = "~/Views/Shared/_Layout_Seller.cshtml";
                Seller seller = (Seller)Session["seller"];
                ViewBag.Seller = seller;
            }
            else //Guest
            {
                ViewBag.Layout = "~/Views/Shared/_Layout.cshtml";
            }
            ViewBag.UserRole = role;

            ViewBag.Manifestations = manifestations;
            ViewBag.Customers = customers;


            return View(tickets);
        }
        private EUserRole CheckRole()
        {
            Administrator admin = (Administrator)Session["admin"];
            if (admin == null || admin.UserRole != EUserRole.ADMINISTRATOR || admin.UserRole.Equals(string.Empty))
            {
                Seller seller = (Seller)Session["seller"];
                if (seller == null || seller.UserRole != EUserRole.SELLER || seller.UserRole.Equals(string.Empty))
                {
                    Customer customer = (Customer)Session["customer"];
                    if (customer == null || customer.UserRole != EUserRole.CUSTOMER || customer.UserRole.Equals(string.Empty))
                    {
                        return EUserRole.GUEST;
                    }
                    return EUserRole.CUSTOMER;
                }
                return EUserRole.SELLER;
            }
            return EUserRole.ADMINISTRATOR;
        }


    }
}
﻿using Projekat.Data;
using Projekat.Models;
using Projekat.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Projekat.Controllers
{
    [RoutePrefix("card")]
    public class ApiCardControlerController : ApiController
    {

        [Route("purchaseCard")]
        [HttpPost]
        public string PurchaseCard([FromBody]TicketViewModel model)
        {
            Dictionary<int, Manifestation> manifestations = (Dictionary<int, Manifestation>)HttpContext.Current.Application["manifestations"];
            Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Current.Application["customers"];
            Dictionary<string, Ticket> tickets = (Dictionary<string, Ticket>)HttpContext.Current.Application["tickets"];

            if (!customers.ContainsKey(model.CustomerID))
                return "No Such cusstomer!";

            if (!manifestations.ContainsKey(model.ManifestationID))
                return "No Such manifestation!";

            Customer customer = new Customer();
            foreach (var item in customers.Values)
            {
                if (item.ID.Equals(model.CustomerID))
                {
                    customer = item;
                    break;
                }
            }

            Manifestation manifestation = new Manifestation();
            foreach (var item in manifestations.Values)
            {
                if (item.ID.Equals(model.ManifestationID))
                {
                    manifestation = item;
                    break;
                }
            }


            //bronze -> discount = 1%
            //silver (<3000<4000) -> discount = 3%
            //gold (4000) -> discount = 5%

            if (model.ManifestationDate.Date > DateTime.Now.Date)
            {
                //It's a later date
                for (int i = 0; i < model.RegularCards; i++)
                {
                    if (customer.NumberOfPoints < 3000)
                    {
                        customer.CustomerType = ECustomerType.BRONZE;
                    }
                    else if (customer.NumberOfPoints >= 3000 && customer.NumberOfPoints < 4000)
                    {
                        customer.CustomerType = ECustomerType.SILVER;
                    }
                    else /*(customer.NumberOfPoints >= 4000)*/
                    {
                        customer.CustomerType = ECustomerType.GOLD;
                    }

                    customer.NumberOfPoints += (model.Price / 1000) * 133;       //Additional point for buying ticker.

                    //Creating the card
                    Ticket newTicket = new Ticket()
                    {
                        TicketID = GenerateID(),
                        ManifestationID = manifestation.ID,
                        ManifestationDate = manifestation.Date,
                        Price = manifestation.TicketPrice,                          //Default ticket price
                        CustomerID = customer.ID,
                        Status = ETicketStatus.RESERVED,                            //Default reserved
                        TicketType = ETicketType.REGULAR,
                        IsDeleted = false
                    };

                    tickets.Add(newTicket.TicketID, newTicket);
                    TicketsData.SaveTickets(tickets);
                }

                for (int i = 0; i < model.FanPitCards; i++)
                {
                    if (customer.NumberOfPoints < 3000)
                    {
                        customer.CustomerType = ECustomerType.BRONZE;
                    }
                    else if (customer.NumberOfPoints >= 3000 && customer.NumberOfPoints < 4000)
                    {
                        customer.CustomerType = ECustomerType.SILVER;
                    }
                    else /*(customer.NumberOfPoints >= 4000)*/
                    {
                        customer.CustomerType = ECustomerType.GOLD;
                    }

                    customer.NumberOfPoints += ((model.Price * 2) / 1000) * 133;       //Additional point for buying ticker.

                    //Creating the card
                    Ticket newTicket = new Ticket()
                    {
                        TicketID = GenerateID(),
                        ManifestationID = manifestation.ID,
                        ManifestationDate = manifestation.Date,
                        Price = manifestation.TicketPrice * 2,                          //Fan Pit ticket price
                        CustomerID = customer.ID,
                        Status = ETicketStatus.RESERVED,                                //Default reserved
                        TicketType = ETicketType.FAN_PIT,
                        IsDeleted = false
                    };

                    tickets.Add(newTicket.TicketID, newTicket);
                    TicketsData.SaveTickets(tickets);
                }


                for (int i = 0; i < model.VipCards; i++)
                {
                    if (customer.NumberOfPoints < 3000)
                    {
                        customer.CustomerType = ECustomerType.BRONZE;
                    }
                    else if (customer.NumberOfPoints >= 3000 && customer.NumberOfPoints < 4000)
                    {
                        customer.CustomerType = ECustomerType.SILVER;
                    }
                    else /*(customer.NumberOfPoints >= 4000)*/
                    {
                        customer.CustomerType = ECustomerType.GOLD;
                    }

                    customer.NumberOfPoints += ((model.Price * 4) / 1000) * 133;       //Additional point for buying ticker.

                    //Creating the card
                    Ticket newTicket = new Ticket()
                    {
                        TicketID = GenerateID(),
                        ManifestationID = manifestation.ID,
                        ManifestationDate = manifestation.Date,
                        Price = manifestation.TicketPrice * 4,                            //VIP ticket price
                        CustomerID = customer.ID,
                        Status = ETicketStatus.RESERVED,                                  //Default reserved
                        TicketType = ETicketType.VIP,
                        IsDeleted = false
                    };

                    tickets.Add(newTicket.TicketID, newTicket);
                    TicketsData.SaveTickets(tickets);

                }


                customers[customer.ID] = customer;
                CustomerData.SaveCustomers(customers);

                manifestations[model.ManifestationID].Capacity -= model.NumberOfCards;
                ManifestationData.SaveManifestations(manifestations);

                return "Successfull";
            }
            else
            {
                return "You can't buy a card for a manifestation that's over";
            }


        }

        [Route("deleteCard")]
        [HttpGet]
        public string Delete([FromUri]string id)
        {
            Dictionary<string, Ticket> tickets = (Dictionary<string, Ticket>)HttpContext.Current.Application["tickets"];
            foreach (var item in tickets.Values)
            {
                if (id.Equals(item.TicketID))
                {
                    item.IsDeleted = true;
                    TicketsData.SaveTickets(tickets);
                    return "Successfull";
                }
            }
            return "Error";
        }

        [Route("unDeleteCard")]
        [HttpGet]
        public string UnDelete([FromUri]string id)
        {
            Dictionary<string, Ticket> tickets = (Dictionary<string, Ticket>)HttpContext.Current.Application["tickets"];
            foreach (var item in tickets.Values)
            {
                if (id.Equals(item.TicketID))
                {
                    item.IsDeleted = false;
                    TicketsData.SaveTickets(tickets);
                    return "Successfull";
                }
            }
            return "Error";
        }

        
        [Route("cancelReservation")]
        [HttpGet]
        public string CancelReservation([FromUri]string id)
        {
            Dictionary<string, Ticket> tickets = (Dictionary<string, Ticket>)HttpContext.Current.Application["tickets"];
            Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Current.Application["customers"];

            Customer customer = new Customer();

            int priceByType = 1;
            

            foreach (var item in tickets.Values)
            {
                if (customers.ContainsKey(item.CustomerID) && id.Equals(item.TicketID))
                {
                    if (customers[item.CustomerID].CustomerType.Equals(ECustomerType.BRONZE))
                        priceByType = 1;
                    else if (customers[item.CustomerID].CustomerType.Equals(ECustomerType.SILVER))
                        priceByType = 2;
                    else //Gold user
                        priceByType = 4;

                    if ((customers[item.CustomerID].NumberOfPoints - (((item.Price * priceByType) / 1000) * 133 * 4)) < 0)
                    {
                        customers[item.CustomerID].NumberOfPoints = 0;
                    }
                    else
                    {
                        customers[item.CustomerID].NumberOfPoints -= ((item.Price * priceByType) / 1000) * 133 * 4;
                    }
                    CustomerData.SaveCustomers(customers);

                    item.Status = ETicketStatus.CANCELED;
                    TicketsData.SaveTickets(tickets);
                    return "Successfull";
                }

                    
            }
            return "Error";
        }

        [Route("unCancelReservation")]
        [HttpGet]
        public string UnCancelReservation([FromUri]string id)
        {
            Dictionary<string, Ticket> tickets = (Dictionary<string, Ticket>)HttpContext.Current.Application["tickets"];
            Dictionary<int, Customer> customers = (Dictionary<int, Customer>)HttpContext.Current.Application["customers"];

            int priceByType = 1;


            foreach (var item in tickets.Values)
            {
                if (customers.ContainsKey(item.CustomerID) && id.Equals(item.TicketID))
                {
                    if (customers[item.CustomerID].CustomerType.Equals(ECustomerType.BRONZE))
                        priceByType = 1;
                    else if (customers[item.CustomerID].CustomerType.Equals(ECustomerType.SILVER))
                        priceByType = 2;
                    else //Gold user
                        priceByType = 4;

                    customers[item.CustomerID].NumberOfPoints += ((item.Price * priceByType) / 1000) * 133;
                    CustomerData.SaveCustomers(customers);

                    item.Status = ETicketStatus.RESERVED;
                    TicketsData.SaveTickets(tickets);
                    return "Successfull";
                }
            }
            return "Error";
        }

        private string GenerateID()
        {
            return Guid.NewGuid().ToString("N").Substring(0, 10);
        }
    }
}
